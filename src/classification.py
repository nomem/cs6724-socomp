import argparse
import utility
from time import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

# utility
import re
import numpy as np
import pandas as pd
from pprint import pprint

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from gensim.models.wrappers import ldamallet

# spacy for lemmatization
import spacy
import lda_calculation

# Enable logging for gensim - optional
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)

import pickle

dataframe = pd.read_csv('output/dataframe_lem.csv', sep='\t', index_col=0)
print "done loading dataframe ..."
msk = np.load('output/mask.npy')
id2word = corpora.Dictionary.load("output/lda_dict.pkl")
print "done loading dictionary ..."
corpus = list(corpora.MmCorpus("output/lda_corpus.mm"))
corpus = np.asarray(corpus)
print "done loading corpus ..."
# for classification
lda_mallet = gensim.models.wrappers.LdaMallet("Mallet/mallet-2.0.8/bin/mallet", corpus=corpus[msk], num_topics=26, id2word=id2word, workers=52, prefix="tmp/infer")
lda_model = ldamallet.malletmodel2ldamodel(lda_mallet)
output = open("output/infer_gensim_26_ldamodel.pkl", 'wb')
pickle.dump([lda_model], output)
print "done classification model ..."
# for difference in mainstream
msk_mainstream = (dataframe["category"] == 1)
lda_mallet_m = gensim.models.wrappers.LdaMallet("Mallet/mallet-2.0.8/bin/mallet", corpus=corpus[msk_mainstream], num_topics=26, id2word=id2word, workers=52, prefix="tmp/m")
output = open("output/mainstream_gensim_26.pkl", 'wb')
pickle.dump([lda_mallet_m], output)
print "done mainstream model ..."
# for difference in nonmainstream
msk_nonmainstream = (dataframe["category"] == 0)
lda_mallet_n = gensim.models.wrappers.LdaMallet("Mallet/mallet-2.0.8/bin/mallet", corpus=corpus[msk_nonmainstream], num_topics=26, id2word=id2word, workers=52, prefix="tmp/n")
output = open("output/nonmainstream_gensim_26.pkl", 'wb')
pickle.dump([lda_mallet_n], output)
print "done nonmainstream model ..."