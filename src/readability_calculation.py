import numpy as np
import multiprocessing
from multiprocessing import Pool
from collections import Counter
import utility
import argparse
from textstat.textstat import textstat


def flesh_score_dataframe(dataframe):
    data_list = dataframe['clean_text'].values.tolist()
    result_list = []
    for data in data_list:
        val = 0
        try:
            val = int(textstat.flesch_kincaid_grade(data))
        except:
            continue
        result_list.append(val)
    return Counter(result_list)


def parallelize_dataframe(dataframe, func):
	num_cores = multiprocessing.cpu_count()
	df_split = np.array_split(dataframe, num_cores)
	pool = Pool(num_cores)
	counter_list = pool.map(func, df_split)
	pool.close()
	pool.join()
	dict_ = Counter()
	for c in counter_list:
		dict_ = dict_ + c
	return dict_


def readability_calculator(args):
	mainstream_df, nonmainstream_df = utility.load_cleaned_dataframe_from_csv(args.data_dir+"/"+args.input_file)
	print("done loading dataframe ...")
	mainstream_flesch = parallelize_dataframe(mainstream_df,flesh_score_dataframe)
	nonmainstream_flesch = parallelize_dataframe(nonmainstream_df,flesh_score_dataframe)
	print("done counting flesch grades ...")


def main():
	parser = argparse.ArgumentParser(description='Optional app description')
	parser.add_argument("-d", "--data-dir", type=str, required=True, help='directory of the input')
	parser.add_argument("-i", "--input-file", type=str, required=True, help='input filename')
	parser.add_argument("-o", "--output-file", type=str, required=True, help='output filename')
	args = parser.parse_args()
	readability_calculator(args)

if __name__ == '__main__':
	main()