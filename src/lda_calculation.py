import argparse
import utility
from time import time
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
from multiprocessing import Pool
from functools import partial
import multiprocessing


# utility
import re
import numpy as np
import pandas as pd
from pprint import pprint

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel

# spacy for lemmatization
import spacy

# Enable logging for gensim - optional
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)

# for saving models
import pickle
import os
from os.path import join
from datetime import datetime


def print_top_words(model, feature_names, n_top_words):
	for topic_idx, topic in enumerate(model.components_):
		message = "Topic #%d: " % topic_idx
		message += " ".join([feature_names[i] for i in topic.argsort()[:-n_top_words - 1:-1]])
		print(message)
	print()


def run_lda_sklearn(args, dataset, n_components=50, n_top_words=10, n_features=None):
	tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2, max_features=n_features, stop_words='english')
	tf = tf_vectorizer.fit_transform(dataset)
	lda = LatentDirichletAllocation(n_components=n_components, max_iter=5, learning_method='online', learning_offset=50., random_state=0)
	lda.fit(tf)
	print("perplexity: %f" % (lda.perplexity(tf)))
	tf_feature_names = tf_vectorizer.get_feature_names()
	print_top_words(lda, tf_feature_names, n_top_words)
	output = open(join(args.data_dir,args.output_file+"_sk.pkl"), 'wb')
	pickle.dump([lda, tf_vectorizer, tf], output)



def sent_to_words(sentences):
	words=[]
	for sentence in sentences:
		words.append((simple_preprocess(str(sentence), deacc=True)))
	return words


def clean_lda(dataframe, stop_words, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
	nlp = spacy.load('en', disable=['parser', 'ner'])
	dataset = sent_to_words(dataframe["clean_text"].values.tolist())
	bigram = gensim.models.Phrases(dataset, min_count=5, threshold=100)
	bigram_mod = gensim.models.phrases.Phraser(bigram)
	col = []
	for index, row in dataframe.iterrows():
		data_words = [word for word in simple_preprocess(str(row["clean_text"])) if word not in stop_words]
		# print " ".join([token.lemma_ for token in nlp(" ".join(bigram_mod[data_words])) if token.pos_ in allowed_postags])
		col.append(" ".join([token.lemma_ for token in nlp(unicode(" ".join(bigram_mod[data_words]))) if token.pos_ in allowed_postags]))
	dataframe["clean_text_lem"] = col
	return dataframe

def parallelize_dataframe(dataframe, func, stop_words):
	num_cores = multiprocessing.cpu_count()
	df_split = np.array_split(dataframe, num_cores)
	pool = Pool(num_cores)
	data = pd.concat(pool.map(partial(func, stop_words=stop_words), df_split))
	pool.close()
	pool.join()
	return data


def preprocess_for_lda(args):
	from nltk.corpus import stopwords
	stop_words = stopwords.words('english')
	mainstream_df, nonmainstream_df = utility.load_cleaned_dataframe_from_csv(args.data_dir+"/"+args.input_file)
	print "done loading dataframe ..."
	dataframe = pd.concat([mainstream_df, nonmainstream_df])
	dataframe_lem = parallelize_dataframe(dataframe, clean_lda, stop_words)
	print "done processing ..."
	dataframe_lem[["clean_text_lem","category"]].to_csv(args.data_dir+"/"+args.output_file,sep="\t")

def compute_coherence_values(args, id2word, corpus, texts, limit, start=2, step=3):
	"""
	Compute c_v coherence for various number of topics

	Parameters:
	----------
	dictionary : Gensim dictionary
	corpus : Gensim corpus
	texts : List of input texts
	limit : Max num of topics

	Returns:
	-------
	model_list : List of LDA topic models
	coherence_values : Coherence values corresponding to the LDA model with respective number of topics
	"""
	for num_topics in range(start, limit, step):
		model = gensim.models.wrappers.LdaMallet(args.mallet_path, corpus=corpus, num_topics=num_topics, id2word=id2word, workers=52, prefix="./tmp/optimal")
		coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=id2word, coherence='c_v')
		output = open(join(args.data_dir,args.output_file+"_gensim_"+str(num_topics)+".pkl"), 'wb')
		pickle.dump([model,coherencemodel.get_coherence()], output)
		print "Num topics: ", num_topics, "calculated at: ",str(datetime.now())





def single_pass_visualization(corpus, id2word, data_lemmatized, args, num_topics=20):
	lda_model = None
	if args.gensim_model == "mallet":
		lda_model = gensim.models.wrappers.LdaMallet(args.mallet_path, corpus=corpus, num_topics=num_topics, id2word=id2word, workers=52, iterations=100)
		pprint(lda_model.show_topics(formatted=False))
	else:
		lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,id2word=id2word,num_topics=num_topics, random_state=100, update_every=1, chunksize=100, passes=10, alpha='auto', per_word_topics=True)
		pprint(lda_model.print_topics())
		doc_lda = lda_model[corpus]
		print('\nPerplexity: ', lda_model.log_perplexity(corpus))
	coherence_model_lda = CoherenceModel(model=lda_model, texts=data_lemmatized, dictionary=id2word, coherence='c_v')
	coherence_lda = coherence_model_lda.get_coherence()
	print('\nCoherence Score: ', coherence_lda)
	output = open(join(args.data_dir,args.output_file+"_gensim_"+str(num_topics)+".pkl"), 'wb')
	pickle.dump([lda_model,coherence], output)
	# for visualizing in notebook
	# pyLDAvis.enable_notebook()
	# import pyLDAvis
	# import pyLDAvis.gensim
	# vis = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)
	# vis

def optimal_topic_calculation(corpus, id2word, data_lemmatized, args):
	compute_coherence_values(args, id2word=id2word, corpus=corpus, texts=data_lemmatized, start=56, limit=100, step=6)
	# plot in jupyter
	# limit = 40
	# start = 2
	# step = 6
	# import matplotlib.pyplot as plt
	# x = range(start, limit, step)
	# plt.plot(x, coherence_values)
	# plt.xlabel("Num Topics")
	# plt.ylabel("Coherence score")
	# plt.legend(("coherence_values"), loc='best')
	# plt.show()
	# prev = 0
	# optimal_idx = -1
	# pos = 0
	# for m, cv in zip(x, coherence_values):
	# 	print("Num Topics =", m, " has Coherence Value of", round(cv, 4))
	# 	if cv>prev:
	# 		prev = cv
	# 		optimal_idx = pos
	# 	pos += 1

	# optimal_model = model_list[optimal_idx]
	# model_topics = optimal_model.show_topics(formatted=False)
	# pprint(optimal_model.print_topics(num_words=10))


def run_lda_gensim(dataset, args):
	data_lemmatized = [sent.split() for sent in dataset]
	print "done splitting ..."
	id2word = None
	if os.path.exists(join(args.data_dir,"lda_dict.pkl")):
		id2word = corpora.Dictionary.load(join(args.data_dir,"lda_dict.pkl"))
	else:
		# takes around 30min
		id2word = corpora.Dictionary(data_lemmatized)
		id2word.filter_extremes(no_below=50, no_above=0.1)
		id2word.save(join(args.data_dir,"lda_dict.pkl"))
	print "done dictionary creation ..."

	if os.path.exists(join(args.data_dir,"lda_corpus.mm")):
		# fast loading with list takes 3 min. Is it necessary?
		corpus = list(corpora.MmCorpus(join(args.data_dir,"lda_corpus.mm")))
	else:
		# takes 12-15min and saving takes another 10min
		corpus = [id2word.doc2bow(text) for text in data_lemmatized]
		corpora.MmCorpus.serialize(join(args.data_dir,"lda_corpus.mm"), corpus)
	print "done corpus creation ..."
	if args.type == "visual":
		single_pass_visualization(corpus, id2word, data_lemmatized, args)
	else:
		optimal_topic_calculation(corpus, id2word, data_lemmatized, args)


def lda_calculator(args):
	mainstream_df, nonmainstream_df = utility.load_cleaned_dataframe_from_csv(args.data_dir+"/"+args.input_file)
	print("done loading dataframe ...")
	if args.model == "sklearn":
		run_lda_sklearn(args, mainstream_df["clean_text_lem"].values.tolist()+nonmainstream_df["clean_text_lem"].values.tolist())
	else:
		run_lda_gensim(mainstream_df["clean_text_lem"].values.tolist()+nonmainstream_df["clean_text_lem"].values.tolist(), args)



def main():
	parser = argparse.ArgumentParser(description='Optional app description')
	parser.add_argument("-d", "--data-dir", type=str, required=True, help='directory of the input')
	parser.add_argument("-i", "--input-file", type=str, required=True, help='input filename')
	parser.add_argument("-c", "--convert", type=bool, default=False, required=False, help='preproess data')
	parser.add_argument("-o", "--output-file", type=str, required=False, help='output filename')
	parser.add_argument("-m", "--model", type=str, default="gensim", required=False, help='model: gensim or sklearn')
	parser.add_argument("-g", "--gensim-model", type=str, default="mallet", required=False, help='gensim model: normal or mallet')
	parser.add_argument("-mp", "--mallet-path", type=str, default="./Mallet/mallet-2.0.8/bin/mallet", required=False, help='path to mallet')
	parser.add_argument("-t", "--type", type=str, default="visual", required=False, help='type: visual or optimal')
	args = parser.parse_args()
	if args.convert:
		preprocess_for_lda(args)
	else:
		lda_calculator(args)

if __name__ == '__main__':
	main()