import pandas
from datetime import datetime, timedelta

def load_cleaned_dataframe_from_csv(filename, sep='\t', nrows=None):
	"""
	input: 
	filename: full path
	sep = file separator
	output: mainstream and nonmainstream dataframe
	"""
	dataframe = pandas.read_csv(filename, sep=sep, index_col=0, nrows=nrows)
	for col in ["clean_text", "date", "clean_text_lem"]:
		if col in dataframe:
			dataframe[col] = dataframe[col].astype(str)
	for col in ["category"]:
		if col in dataframe:
			dataframe[col] = dataframe[col].astype(int)
	mainstream_df = dataframe.loc[dataframe["category"] == 1]
	nonmainstream_df = dataframe.loc[dataframe["category"] == 0]
	return mainstream_df, nonmainstream_df


def save_data_by_week(mainstream_df, nonmainstream_df, loc, start_date=datetime(year=2016, month=8, day=29), end_date=datetime(year=2017, month=2, day=27)):
	cnt = 1
	delta_time = timedelta(days=7)
	upper_bound = timedelta(days=6, hours=23, minutes=59, seconds=59)
	while start_date <= end_date:
		mask = (mainstream_df['date'] >= start_date) & (mainstream_df['date'] <= start_date+upper_bound)
		mainstream_df[mask].to_csv(loc+"/m_"+str(cnt)+".csv", sep='\t')
		mask = (nonmainstream_df['date'] >= start_date) & (nonmainstream_df['date'] <= start_date+upper_bound)
		nonmainstream_df[mask].to_csv(loc+"/n_"+str(cnt)+".csv", sep='\t')
		start_date += delta_time
		cnt += 1