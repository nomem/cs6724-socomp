import json
import re
from os import listdir
from os.path import isfile, join, isdir
from multiprocessing import Pool
from functools import partial
import pandas
import argparse
import sys

sys.path.insert(0,'./lib')
import twokenize

MAINSTREAM_DIRS = ["credible"]

def fix_space_in_hyperlink(text):
	pattern = re.compile(r"/([^/])*\s([^/])*/")
	m = re.search(pattern,text)
	while m:
		i = m.start()
		end = m.end()
		while i <= end and i<len(text):
			if text[i] == ' ':
				text = text[:i]+text[i+1:]
				end -= 1
				i -= 1 
			i += 1
		m = re.search(pattern,text)
	return text


def load_stopwords(directory):
	stopwords = []
	stopword_files = [file for file in listdir(directory) if isfile(join(directory, file))]
	for file in stopword_files:
		f = open(join(directory,file))
		for line in f:
			stopwords.extend(line[:-1].lower())
	return stopwords


def clean_tokens(tokens, stopwords):
	cleaned_tokens = []
	for token in tokens:
		if token[0] == '@' or token[0] == '#' or token[:6] == "https:" or token[:6] == "http:/" or token == "RT":
			pass
		elif token.lower() in stopwords:
			pass
		else:
			cleaned_tokens.append(token)
	return cleaned_tokens


def read_file(path, stopwords, subcat, extended_format=False):
	file = open(path)
	id_str = []
	username = []
	texts = []
	clean_text = []
	hashtags = []
	mentions = []
	favorites = []
	replies = []
	location = []
	date = []
	retweets = []
	category = []
	subcategory = []
	for line in file:
		try:
			js = json.loads(line)
			date.append(js['date'])
			if subcat in MAINSTREAM_DIRS:
				category.append(1)
			else:
				category.append(0)
			# fix space and non-ascii char
			text = fix_space_in_hyperlink(js['text'])
			text = text.encode('ascii', errors='ignore')
			if extended_format:
				id_str.append(js['status_id'])
				username.append(js['username'])
				hashtags.append(js['hashtags'])
				mentions.append(js['mentions'])
				favorites.append(js['favorites'])
				replies.append(js['replies'])
				retweets.append(js['retweets'])
				location.append(js['geo'])
				subcategory.append(subcat)
				texts.append(text)
			text = twokenize.tokenize(text)
			# remove stopwords, hashtag, mentions and urls
			text = clean_tokens(text,stopwords)
			clean_text.append(" ".join(text))
		except Exception as e:
			print(e)
			continue
		
	data_dict = {
		"clean_text":clean_text,
		"date":date,
		"category":category
	}
	if extended_format:
		data_dict = {
			"id":id_str,
			"username":username,
			"text":texts,
			"clean_text":clean_text,
			"hashtags":hashtags,
			"mentions":mentions,
			"favorites":favorites,
			"replies":replies,
			"retweets":retweets,
			"date":date,
			"location":location,
			"subcategory":subcategory,
			"category":category
		}
	dataframe = pandas.DataFrame(data=data_dict)
	return dataframe


def parallel_clean(file, cur_dir, stopwords, extended_format=False):
	print "reading directory %s file: %s" % (cur_dir, file)
	return read_file(join(cur_dir, file), stopwords, cur_dir.split('/')[-2], extended_format)


def preprocess(args):
	stopwords = load_stopwords(args.stopword_dir)
	dataframe_list = []
	dirs = [inode for inode in listdir(args.input_dir) if isdir(join(args.input_dir, inode))]
	for cur_dir in dirs:
		print "reading directory: "+cur_dir
		files = [file for file in listdir(join(args.input_dir, cur_dir)) if isfile(join(join(args.input_dir,cur_dir), file))]
		pool = Pool(processes=56)
		dataframe_list += pool.map(partial(parallel_clean, cur_dir=join(args.input_dir, cur_dir), stopwords=stopwords, extended_format=args.extended_format), files)
	dataframe = pandas.concat(dataframe_list)
	types = dataframe.apply(lambda x: pandas.lib.infer_dtype(x.values))
	columns = ["clean_text","date"]
	if args.extended_format:
		columns = ["id","username","text","clean_text","hashtags","mentions","date","location","subcategory"]
	for col in columns:
		dataframe[col] = dataframe[col].astype(str)
	for col in ["category"]:
		dataframe[col] = dataframe[col].astype(int)
	dataframe.to_csv(join(args.output_dir,args.output_file), sep='\t')


def main():
	parser = argparse.ArgumentParser(description='Optional app description')
	parser.add_argument("-id", "--input-dir", type=str, required=True, help='directory of the input')
	parser.add_argument("-od", "--output-dir", type=str, required=True, help='output directory')
	parser.add_argument("-of", "--output-file", type=str, required=True, help='output filename')
	parser.add_argument("-sd", "--stopword-dir", type=str, required=True, help='stopwords directory')
	parser.add_argument("-ef", "--extended-format", type=bool, default=False, required=False, help='extended will contain all the column')
	args = parser.parse_args()
	preprocess(args)

if __name__ == '__main__':
	main()
