import numpy as np
import multiprocessing
from multiprocessing import Pool
from collections import Counter
import utility
import argparse
from datetime import datetime, timedelta
import pickle
import sys

sys.path.insert(0,'./lib')
import sage


def tokenizer_dataframe(dataframe): 
	return Counter(" ".join(dataframe['clean_text'].values.tolist()).split(" "))

def parallelize_dataframe(dataframe, func):
	num_cores = multiprocessing.cpu_count()
	df_split = np.array_split(dataframe, num_cores)
	pool = Pool(num_cores)
	counter_list = pool.map(func, df_split)
	pool.close()
	pool.join()
	dict_ = Counter()
	for c in counter_list:
		dict_ = dict_ + c
	return dict_

def find_distinct_words(group_dict, base_dict, k=100):
	vocab = [word for word,count in group_dict.most_common(50000)]
	x_group = np.array([group_dict[word] for word in vocab])
	x_base = np.array([base_dict[word] for word in vocab]) + 1.
	mu = np.log(x_base) - np.log(x_base.sum())
	eta = sage.estimate(x_group,mu)
	return sage.topK(eta,vocab, k)


def calc_sage(mainstream_df, nonmainstream_df, k=100):
	mainstream_dict = parallelize_dataframe(mainstream_df,tokenizer_dataframe)
	nonmainstream_dict = parallelize_dataframe(nonmainstream_df,tokenizer_dataframe)
	base_dict = mainstream_dict + nonmainstream_dict
	print("created dictionary ...")
	mainstream_topk = find_distinct_words(mainstream_dict, base_dict, k)
	nonmainstream_topk = find_distinct_words(nonmainstream_dict, base_dict, k)
	return mainstream_topk, nonmainstream_topk

def sage_per_week(args):
	mainstream_df, nonmainstream_df = utility.load_cleaned_dataframe_from_csv(args.data_dir+"/"+args.input_file)
	print("done loading dataframe ...")
	# change indexing
	mainstream_df['date'] = pandas.to_datetime(mainstream_df['date'])
	mainstream_df= mainstream_df.set_index(['date'])
	nonmainstream_df['date'] = pandas.to_datetime(nonmainstream_df['date'])
	nonmainstream_df = nonmainstream_df.set_index(['date'])
	start_date = datetime(year=2016, month=8, day=29)
	delta_time = timedelta(days=7)
	upper_bound = timedelta(days=6, hours=23, minutes=59, seconds=59)
	sage_m = []
	sage_n = []
	while start_date <= datetime(year=2017, month=2, day=27):
		mainstream_topk, nonmainstream_topk = calc_sage(mainstream_df.loc[start_date:start_date+upper_bound], nonmainstream_df.loc[start_date:start_date+upper_bound])
		sage_m.append(mainstream_topk)
		sage_n.append(nonmainstream_topk)
		start_date += delta_time
	with open(args.data_dir+"/"+args.output_file, 'w') as f:
		pickle.dump([sage_m, sage_n], f)

def sage_calculator(args):
	mainstream_df, nonmainstream_df = utility.load_cleaned_dataframe_from_csv(args.data_dir+"/"+args.input_file)
	print "done loading dataframe ..."
	mainstream_topk, nonmainstream_topk = calc_sage(mainstream_df,nonmainstream_df)
	print "done sage calculation ..."
	with open(args.data_dir+"/"+args.output_file, 'w') as f:
		pickle.dump([mainstream_topk, nonmainstream_topk], f)

def main():
	parser = argparse.ArgumentParser(description='Optional app description')
	parser.add_argument("-d", "--data-dir", type=str, required=True, help='directory of the input')
	parser.add_argument("-i", "--input-file", type=str, required=True, help='input filename')
	parser.add_argument("-o", "--output-file", type=str, required=True, help='output filename')
	parser.add_argument("-t", "--by-week", type=bool, default=False, required=False, help='by week')
	args = parser.parse_args()
	if args.by_week:
		sage_per_week(args)
	else:
		sage_calculator(args)

if __name__ == '__main__':
	main()