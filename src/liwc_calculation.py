import numpy as np
import re
import os
from multiprocessing import Pool
from functools import partial
import multiprocessing
import pandas
import argparse
import utility
import sys


sys.path.insert(0, './lib')
import liwc
import RemoteException

DICTIONARIES_PATH = None
FILTERS = {'all':(lambda x:True),
	'first_level':(lambda x: os.path.splitext(x)[0] in 'pronoun article prep auxvb adverbs conj negate'.split(' ') +
		'verbs numbers quant'.split(' ') +
		'posemo negemo'.split(' ') +
		'family friends'.split(' ') +
		'insight cause discrep tentat certain excl'.split(' ') +
		'see hear feel'.split(' ') +
		'body health sexual ingest'.split(' ') +
		['achiev'] +
		'past present future'.split(' ') +
		'motion space time'.split(' ') +
		'work leisure home money religion death'.split(' ') +
		'swear assent nonflu filler'.split(' ')
		)
}



def match_liwc(ptrn, txt):
    try:
	    return len(re.findall(ptrn, txt))
    except:
    	return 0


@RemoteException.showError
def parallel_match(dataframe, ptrns):
	idict = {}
	idx = 0
	ll = []
	df_len = dataframe.shape[0]
	# init
	for pattern in ptrns:
		name, ptrn = pattern
		idict[name] = idx
		ll.append([0.0] * df_len)
		idx += 1
	# work
	idx = 0
	for index, row in dataframe.iterrows():
		length = len(filter(None, row["clean_text"].split()))
		text = row["clean_text"].lower()
		if length == 0:
			idx += 1
			continue
		for pattern in ptrns:
			name, ptrn = pattern
			ll[idict[name]][idx] = float(match_liwc(ptrn,text))/length
		idx += 1
	# assign
	for pattern in ptrns:
		name, ptrn = pattern
		dataframe[name] = ll[idict[name]]
	return dataframe
	

def parallelize_dataframe(dataframe, func, ptrns):
	for pattern in ptrns:
		name, ptrn = pattern
		dataframe[name] = 0.0
	num_cores = multiprocessing.cpu_count() - 4
	df_split = np.array_split(dataframe, num_cores)
	pool = Pool(num_cores)
	data = pandas.concat(pool.map(partial(func, ptrns=ptrns), df_split))
	pool.close()
	pool.join()
	return data

def liwc_calculation(args):
	DICTIONARIES_PATH = args.liwc_dir
	res = liwc.extract_dicts(DICTIONARIES_PATH, FILTERS['all'])
	print "done loading dictionary...."
	mainstream_df, nonmainstream_df = utility.load_cleaned_dataframe_from_csv(args.data_dir+"/"+args.input_file)
	print "done reading dataframe...."
	mainstream_liwc = parallelize_dataframe(mainstream_df,parallel_match,res.items())
	print "done mainstream liwc ..."
	nonmainstream_liwc = parallelize_dataframe(nonmainstream_df,parallel_match,res.items())
	print "done nonmainstream liwc ..."
	pandas.concat([mainstream_liwc,nonmainstream_liwc]).to_csv(args.data_dir+"/"+args.output_file)
	print "done liwc calculation ...."

def main():
	parser = argparse.ArgumentParser(description='Optional app description')
	parser.add_argument("-d", "--data-dir", type=str, required=True, help='directory of the input')
	parser.add_argument("-i", "--input-file", type=str, required=True, help='input filename')
	parser.add_argument("-o", "--output-file", type=str, required=True, help='output filename')
	parser.add_argument("-l", "--liwc-dir", type=str, required=True, help='directory of LIWC dictionary')
	args = parser.parse_args()
	liwc_calculation(args)

if __name__ == '__main__':
	main()