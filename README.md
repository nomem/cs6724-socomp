# CS6724-SoComp #

This repository contains code for CS6724: Social Computing Project. The purpose of this project is to do a longitudinal analysis of the Tweets sharing mainstream and non-mainstream news from Sept 2016 to Feb 2017. There are 37 million tweets collected during this period. We perform both Linguistic and Topical analysis on the dataset.

### Setup ###

Here are the dependencies to run this code:

1. Tweets: Right now we haven't published the data yet. This has to be kept in the data directory.
2. LIWC: You can get the LIWC from [here](http://liwc.wpengine.com/). We used LIWC 2007. This dictionary should be in the LIWC directory.
3. SAGE: This is the calculation of distinct set of words used by different groups. The code is taken from [here](https://github.com/jacobeisenstein/SAGE/tree/master/py-sage).
4. Readability: We measure the readability of Tweets using Flesh-Kincaid method from [textstat](https://pypi.python.org/pypi/textstat).
5. Topic Analysis: We use [LDA](https://en.wikipedia.org/wiki/Latent_Dirichlet_allocation) for Topic analysis.

Here are the steps for the analysis:

```
#clone repo
git clone https://nomem@bitbucket.org/nomem/cs6724-socomp.git
cd cs6724-socomp

# set locations
INPUT_DIR=./data
OUTPUT_DIR=./output
LIWC_DIR=./LIWC
STOPWORD_DIR=./stopwords
MALLET=./Mallet/mallet-2.0.8/bin/mallet

# preprocess data
python src/preprocess.py --input-dir=$INPUT_DIR --output-dir=$OUTPUT_DIR --stopword-dir=$STOPWORD_DIR --output-file=dataframe.csv

# run LIWC
python src/liwc_calculation.py --liwc-dir=$LIWC_DIR --data-dir=$OUTPUT_DIR --input-file=dataframe.csv --output-file=dataframe_temp.csv

# run SAGE
python src/sage_calculation.py --data-dir=$OUTPUT_DIR --input-file=dataframe.csv --output-file=sage.pkl

# run Topic analysis
python src/lda_calculation.py --data-dir=$OUTPUT_DIR --input-file=dataframe.csv --output-file=dataframe_temp.csv

# run readability analysis
python src/readability_calculation.py --data-dir=$OUTPUT_DIR --input-file=dataframe.csv --output-file=dataframe_temp.csv

```


### Contact ###
For any help, contact me at [momen@vt.edu](mailto:momen@vt.edu)