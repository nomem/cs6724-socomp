# coding: utf-8

import re
from multiprocessing import Pool
from functools import partial
import os, sys
import RemoteException

DICTIONARIES_PATH = os.path.join(os.path.dirname(sys.modules[__name__].__file__), "../liwc_dicts")
FILTERS = {'all':(lambda x:True),
			'first_level':(lambda x: os.path.splitext(x)[0] in 'pronoun article prep auxvb adverbs conj negate'.split(' ') +
                                          'verbs numbers quant'.split(' ') +
                                          'posemo negemo'.split(' ') +
                                          'family friends'.split(' ') +
                                          'insight cause discrep tentat certain excl'.split(' ') +
                                          'see hear feel'.split(' ') +
                                          'body health sexual ingest'.split(' ') +
                                          ['achiev'] +
                                          'past present future'.split(' ') +
                                          'motion space time'.split(' ') +
                                          'work leisure home money religion death'.split(' ') +
                                          'swear assent nonflu filler'.split(' ')
				)
}

def extract_dicts(dictionaries_path, the_filter=FILTERS['all'] ):
    res = dict()
    for fname in filter(the_filter, os.listdir(dictionaries_path)):
        dictionary_name = os.path.splitext(fname)[0]
        with open(os.path.join(dictionaries_path, fname)) as f:
            tokens = [r'\b{0}\b'.format(i.strip().replace('*', '.*?')) for i in f]
        dictionary_re = re.compile('|'.join(tokens))
        res[dictionary_name] = dictionary_re
    return res

def match_liwc(ptrn, txt):
    try:
	    return len(re.findall(ptrn, txt))
    except:
    	return 0

@RemoteException.showError
def match_liwcs(name_and_ptrn, txts):
    name, ptrn = name_and_ptrn
    return name, [match_liwc(ptrn, txt) for txt in txts]

def parallel_match_liwcs(names_and_ptrns, txts, n_jobs=7):
    pool = Pool(n_jobs)
    return pool.map(partial(match_liwcs, txts=txts), names_and_ptrns)

if __name__ == '__main__':
    print 'extracting dictionaries'
    res = extract_dicts(DICTIONARIES_PATH, FILTERS['first_level'])
    print 'launching'
    print parallel_match_liwcs(res.items(), 
    	['''what the flying fuck,darn darn darn darn damn damn damn let's do like donald duck damn''', '', None], 
    	7)